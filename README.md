# TYPES 2022 Presentations

Repository gathering the TYPES 2022 presentations.

## How to add your presentation

Please name your presentation in a unambigous way, e.g "AnAmazingLambdaCalculus-JohnDoe.pdf"

- [ ] [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) and make a merge request

## License

All presentations are licenced under CC-BY (https://creativecommons.org/about/cclicenses/)
